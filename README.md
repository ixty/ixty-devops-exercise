Ixty DevOps Interview Excercise
===============================

Provision a local multi-container django-cms install with Vagrant, Docker and 
Ansible.

After running "vagrant up" I can navigate to a webpage displaying the 
django-cms 'Installation Successful' screen.

Requirements:

  1. The page should be served via NGINX.
  2. There must be two seperate containers running django-cms with a WSGI 
     compliant server of your choosing.
  3. There must be a minimum of 4 containers configured as follows:
       a. NGINX
       b. Python / WSGI
       c. Python / WSGI
       d. Postgreql 
  4. The vagrant configuration should work on Linux.
  5. Where possible, Ansible should be used for configuration.

Bonus Considerations:

  1. Portability of vagrant config i.e. make it compatible with Windows / OSX
  2. Logging - aggregation / rotation etc.
  3. Container orchestration and service discovery
